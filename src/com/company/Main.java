package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Программа предназначена для поиска меньшей строки");
        System.out.println("Введите сколько строк вы хотите проверить: ");
        Scanner numberOfStringsIn = new Scanner(System.in);
        int numberOfStrings = numberOfStringsIn.nextInt();
        System.out.println("Введите " + numberOfStrings + " строк(и)");
        String[] arrayOfStrings = new String[numberOfStrings];

        for (int i = 0; i < numberOfStrings; i++) {
            Scanner stringIn = new Scanner(System.in);
            arrayOfStrings[i] = stringIn.nextLine();
        }

        int checkingSmallerString = 1000000;
        int numberOfSmallerString = -1;
        for (int i = 0; i < numberOfStrings; i++) {
            if (arrayOfStrings[i].length() < checkingSmallerString) {
                checkingSmallerString = arrayOfStrings[i].length();
                numberOfSmallerString = i;
            }
        }
        System.out.println("Наменьшая строка: " + arrayOfStrings[numberOfSmallerString] + " Ее длина равна: " + checkingSmallerString);
    }
}
